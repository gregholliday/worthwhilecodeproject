--SQL used to create the SQLLITE database for this project
BEGIN;
CREATE TABLE "courses_instructors" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "first_name" varchar(50) NOT NULL, "last_name" varchar(100) NOT NULL,"email" varchar(254) NULL, "phone" varchar(15) NULL);
CREATE TABLE "courses_courses" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "title" varchar(200) NOT NULL, "description" varchar(200) NOT NULL, "duration" integer NOT NULL, "instructor_id" integer NOT NULL REFERENCES "courses_instructors" ("id"), "art" varchar(100) NULL);
CREATE INDEX "courses_courses_06aab9f3" ON "courses_courses" ("instructor_id");

COMMIT;