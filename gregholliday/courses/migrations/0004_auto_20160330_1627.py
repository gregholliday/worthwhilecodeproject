# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0003_auto_20160330_1135'),
    ]

    operations = [
        migrations.AddField(
            model_name='instructors',
            name='email',
            field=models.EmailField(max_length=254, null=True),
        ),
        migrations.AddField(
            model_name='instructors',
            name='phone',
            field=models.CharField(max_length=15, null=True),
        ),
        migrations.AlterField(
            model_name='courses',
            name='art',
            field=models.ImageField(upload_to='', null=True),
        ),
    ]
