from django.db import models


class Instructors(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(null=True)
    phone = models.CharField(null=True, max_length=15)

    def __str__(self):
        return self.first_name + " " + self.last_name

    def name(self):
        return self.last_name + ", " + self.first_name


class Courses(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    duration = models.IntegerField()
    art = models.ImageField(null=True)
    instructor = models.ForeignKey(Instructors)

    def __str__(self):
        return self.title
