from django.shortcuts import get_object_or_404, render, render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

from .models import Instructors, Courses
from .forms import CourseForm


def index(request):
    course_list = Courses.objects.all()
    context = {'course_list': course_list}
    return render(request, 'index.html', context)


def detail(request, course_id):
    course = get_object_or_404(Courses, pk=course_id)
    return render(request, 'detail.html', {'course': course})


def add(request):
    if request.method == "POST":
        form = CourseForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return HttpResponseRedirect(reverse('courses:detail', kwargs={'course_id': post.id}))
    else:
        form = CourseForm()
    return render(request, 'addupdate.html', {'form': form})


def change(request, course_id):
    course = get_object_or_404(Courses, pk=course_id)
    if request.POST:
        form = CourseForm(request.POST, request.FILES, instance=course)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return HttpResponseRedirect(reverse('courses:detail', kwargs={'course_id': course_id}))
    else:
        form = CourseForm(instance=course)
    return render_to_response('addupdate.html', {'form': form}, context_instance=RequestContext(request))


def instructors(request):
    instructor_list = Instructors.objects.all().order_by('last_name')
    context = {'instructor_list': instructor_list}
    return render(request, 'instructors.html', context)


def instdetail(request, instructor_id):
    instructor = get_object_or_404(Instructors, pk=instructor_id)
    courses = Courses.objects.filter(instructor=instructor_id)
    return render(request,'instructor_detail.html', {'instructor': instructor, 'courses': courses})
