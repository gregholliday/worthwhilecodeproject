from django import forms
from django.utils.safestring import mark_safe
from .models import Courses

DURATION_CHOICES = ((2, '2 Weeks'), (8, '8 weeks'))


# This code snippet was taken from
# https://wikis.utexas.edu/display/~bm6432/Django-Modifying+RadioSelect+Widget+to+have+horizontal+buttons
# it is used to render the radio buttons horizontally rather than vertically
class HorizRadioRenderer(forms.RadioSelect.renderer):
    def render(self):
        return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))


class CourseForm(forms.ModelForm):
    class Meta:
        model = Courses
        fields = ('title', 'description', 'duration', 'instructor', 'art',)
        widgets = {
            'duration': forms.RadioSelect(choices=DURATION_CHOICES, renderer=HorizRadioRenderer)
        }
