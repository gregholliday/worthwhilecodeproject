from django.conf.urls import url

from . import views

app_name = 'courses'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<course_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^add/$', views.add, name='add'),
    url(r'(?P<course_id>[0-9]+)/change/$', views.change, name='change'),
    url(r'^instructors/$', views.instructors, name='instructors'),
    url(r'^(?P<instructor_id>[0-9]+)/instdetail/$',views.instdetail,name='instdetail')
]
