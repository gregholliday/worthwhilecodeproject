from django.contrib import admin

from .models import Instructors, Courses


admin.site.register(Instructors)
admin.site.register(Courses)
