Django Course Catalog Code Project

Version: 1.0

Written by: Greg Holliday

Email: gregholliday@charter.net


This app was written on a Windows 10 machine using Python 3.5.1.
The following modules need to be installed in order to run:

* Pillow ( **pip install Pillow** )
* django-admin-bootstrapped ( **pip install django-admin-bootstrapped** )
* django-widget-tweaks ( **pip install django-widget-tweaks** )

The app also uses Twitter Bootstrap (www.getbootstrap.com) for HTML layout and styling.

The courses app does not require authentication.
To run the Django administration app use the following credentials:

* UserId: admin
* Password: admin

**NOTE:** the images folder in the repository contains several JPG files that were used during testing.